const express = require('express');
const router = express.Router();

const { login } = require('../controllers/auth');

router
  .post('/login', login)
  .get('/check', (req, res) => res.json(req.user))

module.exports = router; 