const express = require('express');
const router = express.Router();
const authVerify = require('../middlewares/auth');

const users = require('./users');
const auth = require('./auth');
const orders = require('./orders');
const clients = require('./clients');

router
  //.use('/auth/login', authVerify, (req, res) => console.log(req.user, 'user'))
  //.use(authVerify)
  .use('/auth', auth)
  .use('/users', users)
  .use('/orders', orders)

module.exports = router; 
