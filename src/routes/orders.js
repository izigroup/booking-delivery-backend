const express = require('express');
const router = express.Router();
const validate = require('../middlewares/validate');
const schemas = require('./validations/schemas');

const { createOrder, getOrders, getOrderById, getOrder, updateOrder } = require('../controllers/orders');

router
  .get('/', getOrders)
  .get('/:id', getOrder)
  .post('/', createOrder)
  .post('/:id', updateOrder)
  .param('id', getOrderById)

module.exports = router; 