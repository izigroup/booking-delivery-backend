const express = require('express');
const router = express.Router();
const validate = require('../middlewares/validate');
const schemas = require('./validations/schemas');

const { createClient } = require('../controllers/clients');

router
  .post('/', validate(schemas.createClient), createClient)

module.exports = router; 