const express = require('express');
const router = express.Router();
const validate = require('../middlewares/validate');
const schemas = require('./validations/schemas');

const { createUser, changePassword } = require('../controllers/users');

router
  .post('/', validate(schemas.createUser), createUser)
  .put('/', changePassword)

module.exports = router; 