const Joi = require('joi');

const schemas = { 
  createUser: Joi.object().keys({ 
    email: Joi.string().email().required(),
    name: Joi.string().required(),
    password: Joi.string().required(),
    role: Joi.string().required(),
    region: Joi.string().required(),
  }),
  createClient: Joi.object().keys({ 
    email: Joi.string().email().required(),
    firstname: Joi.string().required(),
    lastName: Joi.string().required(),
    company: Joi.string().required(),
    type: Joi.string().required(),
    phone: Joi.string().length(10).pattern(/^[0-9]+$/).required(),
  }),
}; 

module.exports = schemas;