const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
  client: { type: Schema.Types.ObjectId, ref: 'clients' },
  units: [{ type: Schema.Types.ObjectId, ref: 'units' }],
  ordering: {},
  onsite: {},
  invoice: {},
  account: {},//{ type: Schema.Types.ObjectId, ref: 'users' },
  status: {type: String, default: 'new'},
  deliveryStatus: {type: String, default: 'not delivered'},
  invoiceStatus: {type: String, default: 'not sent'},
  medialockStatus: {type: String, default: 'locked'},
  onboardingStatus: {type: String, default: 'need contact'},
  region: {type: String, required: true},
  source: String,
  totalCost: {type: Number, required: true},
  discount: {type: Number, required: true},
}, {
  versionKey: false,
  timestamps: true
});

module.exports = Orders = mongoose.model('orders', OrderSchema);