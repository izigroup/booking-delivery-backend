const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UnitSchema = new Schema({
  listingType: { type: String, required: true },
  marketType: { type: String, required: true },
  packageType: { type: String, required: true },
  address: { type: String, required: true },
  number: { type: String, required: true },
  sqft: { type: String, required: true },
  price: { type: String, required: true },
  bedrooms: { type: String, required: true },
  bathrooms: { type: String, required: true },
  services: [],
  tasks: [{ type: Schema.Types.ObjectId, ref: 'tasks' }],
  order: { type: Schema.Types.ObjectId, ref: 'orders' },
}, {
  versionKey: false,
  timestamps: true
});

module.exports = Units = mongoose.model('units', UnitSchema);