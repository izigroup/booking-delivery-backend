const Users = require('./users');
const Clients = require('./clients');
const Orders = require('./orders');
const Tasks = require('./tasks');
const Units = require('./units');

module.exports = {
  Users,
  Clients,
  Orders,
  Tasks,
  Units
};