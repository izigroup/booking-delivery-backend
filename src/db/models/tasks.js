const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TaskSchema = new Schema({
  name: { type: String, required: true },
  status: { type: String, default: 'new' },
  unit: { type: Schema.Types.ObjectId, ref: 'units' },
  assigned: { type: Schema.Types.ObjectId, ref: 'users' },
  qty: { type: String, default: '1' },
  comment: String,
}, {
  versionKey: false,
  timestamps: true
});

module.exports = Tasks = mongoose.model('tasks', TaskSchema);