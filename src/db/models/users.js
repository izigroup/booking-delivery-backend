const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  email: { type: String, required: true },
  password: { type: String, required: true },
  name: { type: String, required: true },
  state: String,
  role: { type: String, required: true },
  region: { type: String, required: true },
}, {
  versionKey: false,
  timestamps: true
});

module.exports = Users = mongoose.model('users', UserSchema);