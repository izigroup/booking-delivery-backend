const bcrypt = require('bcryptjs');

const validateEmail = email => {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

const passwordHash = {
  create: async (password) => {
    const salt = bcrypt.genSaltSync(10);
    const hash = await bcrypt.hash(password,salt)
    return hash;
  },
  compare: async (password, hash) => await bcrypt.compare(password, hash)
};

module.exports = {
  validateEmail,
  passwordHash
}