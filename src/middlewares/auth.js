const jwt = require('jsonwebtoken');
const config = require('config');
const response = require('../utils/response');

const secret = config.get('jwt.secret');

module.exports = (req, res, next) => {
  const token = req.header('authorization');
  if (!token) response(res).error('No token, authorization denied');
  try {
    const decoded = jwt.verify(token, secret);
    req.user = decoded;
    next();
  } catch (e) {
    response(res).error('Token is not valid');
  }
};
