const asyncHandler = require('express-async-handler');
const { Tasks } = require('../db/models/index');
const response = require('../utils/response');

const defaultTasks = [{name: 'video'}, {name: 'photos'}, {name: 'virtual tour'}];

const createTask = async (req, res) => {
  const tasks = await Tasks.create(query);
  response(res).success(tasks);
};

const createTasks = async (units) => {
  const tasks = units.reduce((result, current) => {
    const addTasks = current.services.map(item => ({ ...item, unit: current._id }));
    const updatedDefaultTasks = defaultTasks.map(item => ({ ...item, unit: current._id }));
    return [ ...result, ...addTasks, ...updatedDefaultTasks ];
  }, []);
  return Tasks.insertMany(tasks);
};

module.exports = {
  createTask: asyncHandler(createTask),
  createTasks: asyncHandler(createTasks),
};