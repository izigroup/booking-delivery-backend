const asyncHandler = require('express-async-handler');
const { Users } = require('../db/models/index');
const response = require('../utils/response');
const { passwordHash } = require('../utils/helpers');

const createUser = async (req, res) => {
  const query = { ...req.body };
  query.password = await passwordHash.create(req.body.password);
  const user = await Users.create(query);
  response(res).success(user);
};

//Users.remove().then(res => console.log(res))
//Users.find().then(res => console.log(res))

const changePassword = async (req, res) => {
  const { email, oldPassword, newPassword } = req.body;
  const user = await Users.findOne({ email: email });
  const equals = await passwordHash.compare(oldPassword, user.password);
  if (!equals) response(res).error('Invalid password');
  const password = await passwordHash.create(newPassword);
  return Users.findOneAndUpdate({ email: email }, {password: password}, { new: true, upser: true })
    .then(() => response(res).success('Password changed'))
};

module.exports = {
  createUser: asyncHandler(createUser),
  changePassword: asyncHandler(changePassword),
};