const asyncHandler = require('express-async-handler');
const { Orders, Clients, Tasks, Users, Units } = require('../db/models/index');
const response = require('../utils/response');
const { createClient } = require('./clients');
const { createUnits } = require('./units');

const getOrderById = async (req, res, next, id) => {
  const order = await Orders.findById({ _id: id })
    .populate('client')
    .populate({ path: 'units', populate: { path: 'tasks' } })
    .orFail();
  req.data = order;
  next();
};

const getOrder = (req, res) => {
  response(res).success(req.data);
};

const getOrders = async (req, res) => {
  const orders = await Orders.find()
    .populate('client')
    .populate({ path: 'units', populate: { path: 'tasks' } })
  response(res).success(orders)
};

const createOrder = async (req, res) => {
  const client = await Clients.findOneAndUpdate({ firstName: req.body.ordering.firstName, lastName: req.body.ordering.lastName }, { ...req.body.ordering }, { upsert: true, new: true });
  const order = await Orders.create({ client: client._id, ...req.body });
  const units = await createUnits(order, req.body.listing);
  const updatedOrder = await Orders.findOneAndUpdate({ _id: order._id }, { units: units }, {upsert: true, new: true})
    .populate('client')
    .populate({ path: 'units', populate: { path: 'tasks' } })
  console.log(updatedOrder);
  response(res).success(updatedOrder);
};

const updateOrder = async (req, res) => {
  const updatedOrder = await Orders.findOneAndUpdate({_id: req.data._id}, req.body, { new: true, upser: true })
    .populate('client')
    .populate({ path: 'units', populate: { path: 'tasks' } })
  response(res).success(updatedOrder);
};

module.exports = {
  getOrderById: asyncHandler(getOrderById),
  getOrder: asyncHandler(getOrder),
  getOrders: asyncHandler(getOrders),
  createOrder: asyncHandler(createOrder),
  updateOrder: asyncHandler(updateOrder),
};