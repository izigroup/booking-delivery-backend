const asyncHandler = require('express-async-handler');
const { Units } = require('../db/models/index');
const response = require('../utils/response');
const { createTasks } = require('./tasks');

const createUnits = async (order, listing) => {
  const units = await Units.insertMany(listing.map(item => ({ order: order._id, ...item })));
  const tasks = await createTasks(units);
  await Units.bulkWrite(units.map(item => 
    ({
      updateOne: {
        filter: { _id: item._id },
        update: { tasks: tasks.filter(task => task.unit === item._id) },
        upsert: true
      }
    })  
  )).then(res => console.log(res))
  return Units.find({ _id: { $in: units.map(item => item._id) } }).populate('tasks').then(res => res)
};

module.exports = {
  createUnits//: asyncHandler(createUnits),
};