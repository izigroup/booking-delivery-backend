const asyncHandler = require('express-async-handler');
const config = require('config');
const jwt = require('jsonwebtoken');
const { Users } = require('../db/models/index');
const response = require('../utils/response');
const { passwordHash } = require('../utils/helpers');

const secret = config.get('jwt.secret');
const exp = config.get('jwt.exp');

const login = async (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) response(res).error('Fill all fields');

  const user = await Users.findOne({ email }).orFail();
  const equals = await passwordHash.compare(password, user.password);
  if (!equals) response(res).error('Invalid password');

  const userData = {
    id: user._id,
    name: user.name,
    email: user.email,
    role: user.role,
    region: user.region
  };

  const token = jwt.sign(userData, secret, { expiresIn: exp });
  if (!token) response(res).error('Couldnt sign the token');
  const data = {
    token, 
    user: userData
  };
  response(res).success(data);
};

module.exports = {
  login: asyncHandler(login),
};