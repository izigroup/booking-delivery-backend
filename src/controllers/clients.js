const asyncHandler = require('express-async-handler');
const { Clients } = require('../db/models/index');
const response = require('../utils/response');

const createClient = async (req, res) => {
  const client = await Clients.create(query);
  response(res).success(client);
};

module.exports = {
  createClient: asyncHandler(createClient),
};